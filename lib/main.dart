import 'package:bc/bloc/bloc+calculator/calcul.dart';
import 'package:bc/bloc/simple_example_bloc/first_screen.dart';
import 'package:bc/provider/app+http+provider/http_widget.dart';
import 'package:bc/provider/app_provider_10min/1_app_widget.dart';
import 'package:bc/provider/provider_go/theme.dart';
import 'package:bc/provider/simple_app+provider/home.screen.dart';
import 'package:bc/task_micha/task_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


import 'bloc/tutorial_bloc/widget_for_bloc.dart';

void main() {
  runApp(MyWidget());
}

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Task_Micha(),
    );
  }
}

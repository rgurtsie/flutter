import 'package:bc/main.dart';

import 'package:bc/task_micha/model/model.dart';
import 'package:bc/task_micha/repository.dart';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import 'bloc_1/task_bloc.dart';
import 'bloc_2/task_2_bloc.dart';

class Task_Micha extends StatelessWidget {
  const Task_Micha({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiRepositoryProvider(
        providers: [RepositoryProvider(create: (context) => Repo_Micha())],
        child: TaskWidget(),
      ),
    );
  }
}

class TaskWidget extends StatelessWidget {
  const TaskWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final repository = RepositoryProvider.of<Repo_Micha>(context);
    return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => TaskBloc(repository)..add(Task_1LoadedEvent()),
          ),
          BlocProvider(
            create: (context) =>
                Task_2Bloc(repository)..add(Task_2LoadedEvent()),
          ),
        ],
        child: Scaffold(
          body: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: 50),
                child: Text(
                  'Задача',
                  style: TextStyle(fontSize: 30, color: Colors.black),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50, right: 290, bottom: 10),
                child: Text('новинки'),
              ),
              SizedBox(
                  height: 200,
                  child: BlocBuilder<TaskBloc, TaskState>(
                    builder: (context, state) {
                      return widget_1(state, context);
                    },
                  )),
              Container(
                margin: EdgeInsets.only(top: 50, right: 250, bottom: 10),
                child: Text('рекомендаций'),
              ),
              SizedBox(
                  height: 200,
                  child: BlocBuilder<Task_2Bloc, Task_2State>(
                    builder: (context, state) {
                      return widget_2(state, context);
                    },
                  )),
            ],
          ),
        ));
  }
}

Widget widget_1(state, context) {
  if (state is Task_1LoadedState) {
    List<Model_Micha> list_1 = state.listData;
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: list_1.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(right: 10),
            height: 100,
            width: 200,
            decoration: BoxDecoration(
                color: Colors.amber[50],
                borderRadius: BorderRadius.circular(25),
                border: Border.all(color: Colors.purple)),
            child: Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Container(),
                IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.favorite,
                        color: list_1[index].favor == false
                            ? Colors.white
                            : Colors.black))
              ]),
              Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Center(child: Text(list_1[index].title)))
            ]),
          );
        });
  } else if (state is Task_1ErrorLoadedState) {
    return Center(child: Text('You receive an error message!'));
  } else if (state is Task_1LoadingState) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
  return Container();
}

Widget widget_2(state, context) {
  if (state is Task_2LoadedState) {
    List<Model_Micha> list_2 = state.listData;
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: list_2.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(right: 10),
            height: 100,
            width: 200,
            decoration: BoxDecoration(
                color: Colors.amber[50],
                borderRadius: BorderRadius.circular(25),
                border: Border.all(color: Colors.purple)),
            child: Column(children: [
              Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Container(),
                IconButton(
                    onPressed: () {},
                    icon: Icon(
                      Icons.favorite,
                      color: list_2[index].favor == false
                          ? Colors.white
                          : Colors.black,
                    ))
              ]),
              Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Center(child: Text(list_2[index].title)))
            ]),
          );
        });
  } else if (state is Task_2ErrorLoadedState) {
    return Center(child: Text('You receive an error message!'));
  } else if (state is Task_2LoadingState) {
    return Center(child: CircularProgressIndicator());
  }
  return Container();
}

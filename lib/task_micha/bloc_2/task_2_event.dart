part of 'task_2_bloc.dart';

abstract class Task_2Event extends Equatable {
  const Task_2Event();

  @override
  List<Object> get props => [];
}
class Task_2LoadedEvent extends Task_2Event {}
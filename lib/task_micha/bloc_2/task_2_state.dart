part of 'task_2_bloc.dart';

abstract class Task_2State extends Equatable {
  const Task_2State();

  @override
  List<Object> get props => [];
}

class Task_2LoadingState extends Task_2State {}

class Task_2LoadedState extends Task_2State {
  final List<Model_Micha> listData;

  Task_2LoadedState(this.listData);
}

class Task_2ErrorLoadedState extends Task_2State {}

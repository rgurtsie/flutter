import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model/model.dart';
import '../repository.dart';

part 'task_2_event.dart';
part 'task_2_state.dart';

class Task_2Bloc extends Bloc<Task_2Event, Task_2State> {
  final Repo_Micha _repo_micha;
  Task_2Bloc(this._repo_micha) : super(Task_2LoadingState()) {
    on<Task_2LoadedEvent>(_onTask_2Event);
  }
  _onTask_2Event(Task_2Event event, Emitter emit) async {
    final data = _repo_micha.list_2;
    emit(Task_2LoadingState());
    await Future.delayed(Duration(seconds: 2));

    emit(Task_2LoadedState(data));
  }
}

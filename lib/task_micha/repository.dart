import 'model/model.dart';

class Repo_Micha {
  List<Model_Micha> list_1 = [
    Model_Micha(id: 1, title: 'молоко', favor: false),
    Model_Micha(id: 2, title: 'хлеб', favor: false),
    Model_Micha(id: 3, title: 'кола', favor: false),
    Model_Micha(id: 4, title: 'яблоко', favor: false),
  ];
  List<Model_Micha> list_2 = [
    Model_Micha(id: 1, title: 'Табуретка', favor: false),
    Model_Micha(id: 2, title: 'хлеб', favor: false),
    Model_Micha(id: 3, title: 'яблоко', favor: false),
    Model_Micha(id: 4, title: 'что-то непонятное', favor: false),
  ];
}

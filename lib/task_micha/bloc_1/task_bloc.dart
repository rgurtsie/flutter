import 'package:bc/task_micha/repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model/model.dart';

part 'task_event.dart';
part 'task_state.dart';

class TaskBloc extends Bloc<TaskEvent, TaskState> {
  final Repo_Micha _repo_micha;
  TaskBloc(this._repo_micha) : super(Task_1LoadingState()) {
    on<Task_1LoadedEvent>(_onTask_1LoadedEvent);
  }
  _onTask_1LoadedEvent(Task_1LoadedEvent event, Emitter emit) async {
    final data = _repo_micha.list_1;
    emit(Task_1LoadingState());
    await Future.delayed(Duration(seconds: 2));

    emit(Task_1LoadedState(data));
  }
}

part of 'task_bloc.dart';

abstract class TaskState extends Equatable {
  const TaskState();

  @override
  List<Object> get props => [];
}

class Task_1LoadingState extends TaskState {}

class Task_1LoadedState extends TaskState {
  final List<Model_Micha> listData;

  Task_1LoadedState(this.listData);
}

class Task_1ErrorLoadedState extends TaskState {}

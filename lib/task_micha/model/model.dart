class Model_Micha {
  final String title;
  final bool favor;
  final int id;

  Model_Micha({required this.title, required this.favor, required this.id});

  factory Model_Micha.fromJson(Map<String, dynamic> json) {
    return Model_Micha(
        title: json['title'], favor: json['favor'], id: json['id']);
  }
}

import 'dart:convert';

import 'package:http/http.dart';

class AddRepository {
  Uri api = Uri.parse('https://reqres.in/api/users?page=2');

  Future getAddData() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List listData = jsonDecode(response.body)['data'];
      return listData.map(((data) => AddModel.fromJson(data))).toList();
    }
  }
}

class AddModel {
  final int id;
  final String email;

  AddModel({required this.id, required this.email});

  factory AddModel.fromJson(Map<String, dynamic> json) {
    return AddModel(id: json['id'], email: json["email"]);
  }
}

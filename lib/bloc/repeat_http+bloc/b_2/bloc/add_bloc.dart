
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../repository/add_repository.dart';

part 'add_event.dart';
part 'add_state.dart';

class AddBloc extends Bloc<AddEvent, AddState> {
  final AddRepository _repository;
  AddBloc(this._repository) : super(AddInitialState()) {
    on<AddLoadedEvent>((event, emit) async {
      emit(AddLoadingState('Please, Wait it !'));

      try {
        final List<AddModel> listData = await _repository.getAddData();
        emit(AddLoadedState(listData));
      } catch (errorText) {

        emit(AddErrorLoadingState("You are Wrong !"));
      }
    });
  }
}

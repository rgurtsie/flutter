part of 'add_bloc.dart';

abstract class AddState extends Equatable {
  const AddState();

  @override
  List<Object> get props => [];
}

class AddInitialState extends AddState {}

class AddLoadingState extends AddState {
  final String loadingText;

  AddLoadingState(this.loadingText);
  @override
  List<Object> get props => [loadingText];
}

class AddLoadedState extends AddState {
  final List<AddModel> listData;

  AddLoadedState(this.listData);
  @override
  List<Object> get props => [listData];
}

class AddErrorLoadingState extends AddState {
  final String errorText;

  AddErrorLoadingState(this.errorText);
  @override
  List<Object> get props => [errorText];
}

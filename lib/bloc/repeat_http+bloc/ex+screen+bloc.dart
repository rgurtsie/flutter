
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'b_1/bloc/repeat_bloc.dart';
import 'b_1/bloc/repository+model.dart';
import 'b_2/bloc/add_bloc.dart';
import 'b_2/repository/add_repository.dart';

class Wrapper_ScreenBloc extends StatelessWidget {
  const Wrapper_ScreenBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MultiRepositoryProvider(
        providers: [
          RepositoryProvider(
            create: (context) => Repository(),
          ),
          RepositoryProvider(
            create: (context) => AddRepository(),
          ),
        ],
        child: const ScreenBloc(),
      ),
    );
  }
}

class ScreenBloc extends StatelessWidget {
  const ScreenBloc({super.key});

  @override
  Widget build(BuildContext context) {
    final repository = RepositoryProvider.of<Repository>(context);
    final addRepository = RepositoryProvider.of<AddRepository>(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => RepeatBloc(repository)..add(RepeatLoadedEvent()),
        ),
        BlocProvider(
          create: (context) => AddBloc(addRepository)..add(AddLoadedEvent()),
        ),
      ],
      child: Scaffold(
        body: Column(
          children: [
            BlocBuilder<RepeatBloc, RepeatState>(
              builder: (context, state) {
                if (state is RepeatLoadingState) {
                  return Center(
                      child: Text(
                    state.loadingText,
                    style: TextStyle(
                        fontSize: 50,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ));
                } else if (state is RepeatLoadedState) {
                  List<UM> listProvider = state.listUser;
                  return Expanded(
                    child: ListView.builder(
                        itemCount: listProvider.length,
                        itemBuilder: (context, index) => Card(
                              elevation: 4,
                              child: ListTile(
                                  leading: CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        listProvider[index].avatar),
                                  ),
                                  subtitle: Text(
                                      "${listProvider[index].first_name} ${listProvider[index].last_name}")),
                            )),
                  );
                } else if (state is RepeatErrorLoadingState) {
                  return Center(
                      child: Text(
                    state.errorText,
                    style: TextStyle(
                        fontSize: 50,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ));
                }
                return Container();
              },
            ),
            Container(
              height: 30,
            ),
            BlocBuilder<AddBloc, AddState>(builder: (context, state) {
              if (state is AddLoadingState) {
                return Center(child: Text(state.loadingText));
              } else if (state is AddLoadedState) {
                List<AddModel> listUser = state.listData;
                return Expanded(
                  child: ListView.builder(
                    itemCount: listUser.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => Container(
                        height: 100,
                        color: Colors.green,
                        margin: EdgeInsets.only(right: 10),
                        child: Column(
                          children: [
                            Text(listUser[index].id.toString()),
                            Text(listUser[index].email),
                          ],
                        )),
                  ),
                );
              } else if (state is AddErrorLoadingState) {
                return Center(child: Text(state.errorText));
              }
              return Container();
            }),
          ],
        ),
      ),
    );
  }
}

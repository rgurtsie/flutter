part of 'repeat_bloc.dart';

abstract class RepeatEvent extends Equatable {
  const RepeatEvent();
}

class RepeatLoadedEvent extends RepeatEvent {

    @override
  List<Object> get props => [];
}


import 'package:bc/bloc/repeat_http+bloc/b_1/bloc/repository+model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';


part 'repeat_event.dart';
part 'repeat_state.dart';

class RepeatBloc extends Bloc<RepeatEvent, RepeatState> {
  final Repository _repository;

  RepeatBloc(this._repository) : super(RepeatIinitialState()) {
    on<RepeatLoadedEvent>((event, emit) async {
      emit(RepeatLoadingState('Please, wait it'));

      try {
        final users = await _repository.getData();
        emit(RepeatLoadedState(users));
      
      } catch (error) {
        emit(RepeatErrorLoadingState("You got Error: ${error} !"));
      }
    });
  }
}

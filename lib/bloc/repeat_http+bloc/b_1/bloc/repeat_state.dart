part of 'repeat_bloc.dart';

abstract class RepeatState extends Equatable {
  const RepeatState();

  @override
  List<Object> get props => [];
}

class RepeatIinitialState extends RepeatState {}

class RepeatLoadingState extends RepeatState {
  final String loadingText;

  RepeatLoadingState(this.loadingText);
    @override
  List<Object> get props => [loadingText];
}

class RepeatLoadedState extends RepeatState {
  final List<UM> listUser;

  RepeatLoadedState(this.listUser);
    @override
  List<Object> get props => [listUser];
}

class RepeatErrorLoadingState extends RepeatState {
  final String errorText;

  RepeatErrorLoadingState(this.errorText);
    @override
  List<Object> get props => [errorText];
}

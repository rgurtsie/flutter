import 'dart:convert';

import 'package:http/http.dart';

class Repository {
  Uri api = Uri.parse('https://reqres.in/api/users?page=2');

  Future getData() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List listUser = jsonDecode(response.body)['data'];

      return listUser.map(((data) => UM.fromJson(data))).toList();
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}

class UM {
  final String first_name;
  final String last_name;
  final String avatar;

  UM({required this.first_name, required this.last_name, required this.avatar});

  factory UM.fromJson(Map<String, dynamic> json) {
    return UM(
        first_name: json['first_name'] ??  "First Name",
        last_name: json['last_name']?? "Last Name",
        avatar: json['avatar'] ?? "https://reqres.in/img/faces/8-image.jpg");
  }
}

import 'dart:convert';



import 'package:http/http.dart';

class Model_agin {
  final String first_name;
  final String last_name;
  final String avatar;

  Model_agin(
      {required this.first_name,
      required this.last_name,
      required this.avatar});

  factory Model_agin.fromJson(Map<String, dynamic> json) {
    return Model_agin(
        first_name: json['first_name'],
        last_name: json['last_name'],
        avatar: json['avatar']);
  }
}

class Repository_again {
  Uri api = Uri.parse('https://reqres.in/api/users?page=2');
  Future getData() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List request = jsonDecode(response.body)['data'];
      return request.map(((data) => Model_agin.fromJson(data))).toList();
    } else {
      return Exception(response.reasonPhrase);
    }
  }
}

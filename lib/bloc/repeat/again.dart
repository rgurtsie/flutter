import 'package:bc/bloc/repeat/repository/again_repository.dart';


import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

import 'bloc/again_bloc.dart';

class MyAgain extends StatelessWidget {
  const MyAgain({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: RepositoryProvider(
        create: (BuildContext context) {
          return Repository_again();
        },
        child: AgainWidget(),
      ),
    );
  }
}

class AgainWidget extends StatelessWidget {
  const AgainWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          AgainBloc(RepositoryProvider.of(context))..add(AgainEvent()),
      child: Scaffold(body: BlocBuilder<AgainBloc, AgainState>(
        builder: (context, state) {
          return widget(state);
        },
      )),
    );
  }

  Widget widget(state) {
    if (state is AgainLoadingState) {
      return state.widget;
    } else if (state is AgainLoadedState) {
      List<Model_agin> userData = state.listData;
      return 
           ListView.builder(
              itemCount: userData.length,
              itemBuilder: (context, index) => Card(
                    child: ListTile(
                        subtitle: Text(userData[index].first_name +
                            ' ' +
                            userData[index].last_name),
                        leading: CircleAvatar(
                          backgroundImage: NetworkImage(userData[index].avatar),
                        )),
                  ));
    } else if (state is AgainErrorLoadingState) {
      return Center(child: Text(state.error));
    }
    return Container();
  }
}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../repository/again_repository.dart';

part 'again_event.dart';
part 'again_state.dart';

class AgainBloc extends Bloc<AgainEvent, AgainState> {
 final Repository_again _repository_again;

  AgainBloc(this._repository_again) : super(AgainInitialState()) {
    on<AgainEvent>((AgainEvent event, Emitter<AgainState> emit) async {

      Widget widget = Center(child: CircularProgressIndicator());
      emit(AgainLoadingState(widget));

      try {
        final lsitData = await _repository_again.getData();
        emit(AgainLoadedState(lsitData));
      } catch (error) {
        emit(AgainErrorLoadingState("You are wrong !"));
      }
    });
  }
}

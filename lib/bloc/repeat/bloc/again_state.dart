part of 'again_bloc.dart';

abstract class AgainState extends Equatable {}

class AgainInitialState extends AgainState {
    @override
  List<Object> get props => [];
}

class AgainLoadingState extends AgainState {
  final Widget widget;

  AgainLoadingState(this.widget);
  @override
  List<Object> get props => [widget];
}

class AgainLoadedState extends AgainState {
  final List<Model_agin> listData;

  AgainLoadedState(this.listData);
  @override
  List<Object> get props => [listData];
}

class AgainErrorLoadingState extends AgainState {
  final String error;

  AgainErrorLoadingState(this.error);
  @override
  List<Object> get props => [error];
}

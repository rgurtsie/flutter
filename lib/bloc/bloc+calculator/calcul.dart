import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/calculator_bloc.dart';

class MyCalculator extends StatelessWidget {
  const MyCalculator({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => CalculatorBloc(),
        ),
      ],
      child: MaterialApp(home: Calcul()),
    );
  }
}

class Calcul extends StatelessWidget {
  const Calcul({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalculatorBloc, CalculatorState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: Colors.black,
          body: Column(
            children: [
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 100, bottom: 100),
                    child: Column(
                      children: [
                        Text(
                          state.firstNumber,
                            style:
                                TextStyle(color: Colors.white, fontSize: 50)),
                        Text(state.operation,
                            style:
                                TextStyle(color: Colors.white, fontSize: 50)),
                        Text(state.secondNumber,
                            style:
                                TextStyle(color: Colors.white, fontSize: 50)),
                        Divider(color: Colors.green),
                        Text(state.mathResult,
                            style: TextStyle(color: Colors.white, fontSize: 50))
                      ],
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        return context
                            .read<CalculatorBloc>()
                            .add(RemoveEvent());
                      },
                      child: Text('AC')),
                  ElevatedButton(
                      onPressed: () {
                        return context
                            .read<CalculatorBloc>()
                            .add(ChangeMarkEvent());
                      },
                      child: Text('+/-')),
                  ElevatedButton(
                      onPressed: () => context.read<CalculatorBloc>().add(DeleteLastNumberEvent()),
                      child: Text('del')),
                  ElevatedButton(onPressed: () => context.read<CalculatorBloc>().add(OperationEvent("/")), child: Text('/')),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("7")),
                      child: Text('7')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("8")),
                      child: Text('8')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("9")),
                      child: Text('9')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("X")),
                      child: Text('X')),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("4")),
                      child: Text('4')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("5")),
                      child: Text('5')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("6")),
                      child: Text('6')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("-")),
                      child: Text('-')),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("1")),
                      child: Text('1')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("2")),
                      child: Text('2')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("3")),
                      child: Text('3')),
                  ElevatedButton(
                      onPressed: () =>
                          {},
                      child: Text('+')),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent("0")),
                      child: Text('0')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(AddNumberEvent(".")),
                      child: Text('.')),
                  ElevatedButton(
                      onPressed: () => context
                          .read<CalculatorBloc>()
                          .add(ResultEvent()),
                      child: Text('=')),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

part of 'calculator_bloc.dart';

class CalculatorState extends Equatable {
  final String mathResult;
  final String firstNumber;
  final String secondNumber;
  final String operation;

  CalculatorState(
      {this.mathResult = "",
      this.firstNumber = "12",
      this.secondNumber = "12",
      this.operation = "+"});

  @override
  List<Object> get props => [
        mathResult,
        firstNumber,
        secondNumber,
        operation,
      ];

  CalculatorState copyWith({
    String? mathResult,
    String? firstNumber,
    String? SecondNumber,
    String? operation,
  }) {
    return CalculatorState(
      mathResult: mathResult ?? this.mathResult,
      firstNumber: firstNumber ?? this.firstNumber,
      secondNumber: SecondNumber ?? this.secondNumber,
      operation: operation ?? this.operation,
    );
  }
}

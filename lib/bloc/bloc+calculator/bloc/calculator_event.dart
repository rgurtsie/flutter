part of 'calculator_bloc.dart';

abstract class CalculatorEvent extends Equatable {
  const CalculatorEvent();

  @override
  List<Object> get props => [];
}

class AddNumberEvent extends CalculatorEvent {
  final String number;

  AddNumberEvent(this.number);

  @override
  List<Object> get props => [number];
}

class RemoveEvent extends CalculatorEvent {}

class ChangeMarkEvent extends CalculatorEvent {}

class DeleteLastNumberEvent extends CalculatorEvent {}

class OperationEvent extends CalculatorEvent {
  final String operation;

  OperationEvent(this.operation);
}

class ResultEvent extends CalculatorEvent {}

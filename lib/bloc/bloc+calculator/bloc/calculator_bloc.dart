import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'calculator_event.dart';
part 'calculator_state.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  CalculatorBloc() : super(CalculatorState()) {
    on<AddNumberEvent>(_onAddNumberEvent);
    on<RemoveEvent>(_onRemoveEvent);
    on<ChangeMarkEvent>(_onChangeMarkEvent);
    on<ResultEvent>(_onResult);
    on<DeleteLastNumberEvent>(_onDeleteLastNumberEvent);
    on<OperationEvent>(_onOperationEvent);
  }

  Stream<CalculatorState> _onResult(
      ResultEvent event, Emitter<CalculatorState> emit) async* {
    final double number_1 = double.parse(state.firstNumber);
    final double number_2 = double.parse(state.secondNumber);
    final operation = state.operation;
    final double result;
    if (operation == '+') {
      result = number_1 + number_2;
      // yield state.copyWith(mathResult: result.toString());
      emit(CalculatorState(mathResult: result.toString()));
    } else if (operation == "X") {
      result = number_1 * number_2;
      yield state.copyWith(mathResult: result.toString());
    } else if (operation == "/") {
      result = number_1 / number_2;
      yield state.copyWith(mathResult: result.toString());
    } else if (operation == "-") {
      result = number_1 - number_2;
      yield state.copyWith(mathResult: result.toString());
    }
 
  }



  void _onOperationEvent(OperationEvent event, Emitter<CalculatorState> emit) {
    emit(CalculatorState(
        firstNumber: state.mathResult,
        mathResult: '0',
        operation: event.operation,
        secondNumber: '0'));
  }

  void _onDeleteLastNumberEvent(
      DeleteLastNumberEvent event, Emitter<CalculatorState> emit) {
    emit(CalculatorState().copyWith(
        mathResult: state.mathResult.length > 1
            ? state.mathResult.substring(0, state.mathResult.length - 1)
            : '0'));
  }

  void _onChangeMarkEvent(
      ChangeMarkEvent event, Emitter<CalculatorState> emit) {
    final state = this.state;
    final mathResult = state.mathResult;

    emit(CalculatorState().copyWith(
        mathResult: mathResult.contains('-')
            ? mathResult.replaceFirst('-', '')
            : mathResult.replaceFirst('', '-')));
  }

  void _onAddNumberEvent(AddNumberEvent event, Emitter<CalculatorState> emit) {
    final state = this.state;
    emit(CalculatorState(
        mathResult: state.mathResult == '0'
            ? event.number
            : state.mathResult + event.number,
        firstNumber: state.firstNumber,
        secondNumber: state.secondNumber,
        operation: state.operation));
  }

  void _onRemoveEvent(RemoveEvent event, Emitter<CalculatorState> emit) {
    final state = this.state;
    emit(CalculatorState(
        firstNumber: "", secondNumber: "", mathResult: "", operation: ''));
  }
}

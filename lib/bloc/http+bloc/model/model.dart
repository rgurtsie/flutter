class UserModel {
  final int id;

  final String firstName;
  final String lastName;
  final String avatar;
  final String email;

  UserModel(
      {required this.id,
      required this.firstName,
      required this.lastName,
      required this.avatar,
      required this.email});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['id'],
      email: json["email"],
      firstName: json["firstName"] ?? "First Name",
      lastName: json['lastName'] ?? "Last Name",
      avatar: json["avatar"] ?? "https://reqres.in/img/faces/8-image.jpg",
    );
  }
}

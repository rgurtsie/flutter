
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'model/model.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({super.key, required this.data});

  final UserModel data;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Center(
              child: CircleAvatar(
                maxRadius: 60,
                backgroundImage: NetworkImage(data.avatar),
              ),
            ),
            Text(data.firstName + " " + data.lastName),
            Text(data.email)
          ],
        ),
      )),
    );
  }
}


import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model/model.dart';
import '../repository.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final UserRepository _userRepository;

  UserBloc(this._userRepository) : super(UserLoadingState()) {
    on<UserLoadedEvent>((event, emit) async {
      
      emit(UserLoadingState());
      try {
        final users = await _userRepository.getUsers();
        emit(UserLoadedState(users));
      } catch (e) {
        emit(UserErrorLoadingState(e.toString()));
      }
    });
  }
}

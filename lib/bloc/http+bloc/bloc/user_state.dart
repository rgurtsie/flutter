part of 'user_bloc.dart';

abstract class UserState extends Equatable {}

class UserLoadingState extends UserState {
  
  @override
  List<Object> get props => [];
  
}

class UserLoadedState extends UserState {
  final List<UserModel> users;

  UserLoadedState(this.users);
  @override
  List<Object> get props => [users];
}

class UserErrorLoadingState extends UserState {
  final String error;

  UserErrorLoadingState(this.error);
  @override
  List<Object> get props => [error];
}

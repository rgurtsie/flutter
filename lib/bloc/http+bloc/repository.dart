import 'dart:convert';

import 'package:http/http.dart';

import 'model/model.dart';

class UserRepository {
  Uri api = Uri.parse('https://reqres.in/api/users?page=2');

  Future getUsers() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List result = jsonDecode(response.body)['data'];
      return result.map(((e) {
        return UserModel.fromJson(e);
      })).toList();
    } else {
      throw Exception(response.reasonPhrase);
    }
  }
}

import 'package:bc/bloc/app_bloc_10min/bloc/min10_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MyLook extends StatelessWidget {
  const MyLook({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: RepositoryProvider(
        create: (context) => Model_MIN_10Repository(),
        child: LookScreen(),
      ),
    );
  }
}

class LookScreen extends StatefulWidget {
  const LookScreen({super.key});

  @override
  State<LookScreen> createState() => _LookScreenState();
}

class _LookScreenState extends State<LookScreen> {
  @override
  Widget build(BuildContext context) {
    var movies = context.watch<Min10Bloc>().add(Min10LoadedSpecListEvent());
    // var special_movies =
    //     context.read<Min10Bloc>().add(Min10AddToSpecListEvent());
    return BlocProvider(
      create: (context) =>
          Min10Bloc(RepositoryProvider.of<Model_MIN_10Repository>(context))
            ..add(Min10LoadedSpecListEvent()),
      child: Scaffold(
        body: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 50, bottom: 50),
              child: ElevatedButton(
                  onPressed: () {
                    // if (
                      // special_movies.length == 0
                      // ) {
                    //   return;
                    // } else {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => const Second_Widget()));
                    // }
                  },
                  child: Text("Go to my list ")),
            ),
            BlocBuilder<Min10Bloc, Min10State>(
              builder: (context, state) {
                if (state is Min10LoadedSpecListState) {
                  Expanded(
                      child: ListView.builder(
                          itemCount: state.listData.length,
                          itemBuilder: (context, index) {
                            final currentMovies = state.listData[index];
                            return Card(
                              elevation: 4,
                              child: ListTile(
                                title: Text(currentMovies.title),
                                subtitle: Text(currentMovies.duration),
                                trailing: IconButton(
                                    onPressed: () {
                                      // if (!special_movies
                                      //     .contains(currentMovies)) {
                                      //   context
                                      //       .read<MovieProvider>()
                                      //       .addToList(currentMovies);
                                      // } else {
                                      //   context
                                      //       .read<MovieProvider>()
                                      //       .removeFromList(currentMovies);
                                      // }
                                    },
                                    icon: Icon(Icons.favorite,
                                        color:
                                            // special_movies
                                            //         .contains(currentMovies)
                                            //     ? Colors.red

                                            // :
                                            Colors.white)),
                              ),
                            );
                          }));
                }
                return Container();
              },
            )
          ],
        ),
      ),
    );
  }
}

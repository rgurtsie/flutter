import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model_10min/model_10min.dart';

part 'min10_event.dart';
part 'min10_state.dart';

class Model_MIN_10Repository {
  final List<Model_MIN_10> listData = List.generate(
      100,
      (index) => Model_MIN_10(
          'Move ${index}', "${Random().nextInt(100) + 60} minutes"));
}

class Min10Bloc extends Bloc<Min10Event, Min10State> {
  Model_MIN_10Repository _model_min_10repository;
  Min10Bloc(this._model_min_10repository) : super(Min10LoadingState()) {
    on<Min10LoadedSpecListEvent>(_onMin10LoadedSpecListEvent);
    on<Min10AddToSpecListEvent>(_onMin10AddToSpecListEvent);
  }
  _onMin10LoadedSpecListEvent(Min10LoadedSpecListEvent event, Emitter emit) {
    try {
      // final List<Model_MIN_10> list = listData;
      final List<Model_MIN_10> _movies = _model_min_10repository.listData;
      // List<Model_MIN_10> get movies => _movies;
      emit(Min10LoadedSpecListState(_movies));
    } catch (error) {
      emit(Min10ErrorLoadedSpecListState('You are wrong !'));
    }
  }

  _onMin10AddToSpecListEvent(Min10AddToSpecListEvent event, Emitter emit) {
    // final List<Model_MIN_10> list = listData;
    // List<Model_MIN_10> get movies => list;
    final List<Model_MIN_10> special_movies = [];
    special_movies.add(event.movie);

    emit(Min10AddToSpecListState(special_movies));
  }
}

part of 'min10_bloc.dart';

abstract class Min10State extends Equatable {
  const Min10State();

  @override
  List<Object> get props => [];
}

class Min10LoadingState extends Min10State {}

class Min10LoadedSpecListState extends Min10State {
  final List<Model_MIN_10> listData;

  Min10LoadedSpecListState(this.listData);
}

class Min10ErrorLoadedSpecListState extends Min10State {
  final String error;

  Min10ErrorLoadedSpecListState(this.error);
}

class Min10AddToSpecListState extends Min10State {
 final  List listData;

  Min10AddToSpecListState(this.listData);
}

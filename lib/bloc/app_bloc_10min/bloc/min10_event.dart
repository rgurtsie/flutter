// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'min10_bloc.dart';

abstract class Min10Event extends Equatable {
  const Min10Event();

  @override
  List<Object> get props => [];
}

class Min10LoadedSpecListEvent extends Min10Event {}

class Min10AddToSpecListEvent extends Min10Event {
 final Model_MIN_10 movie;

  Min10AddToSpecListEvent(this.movie);
  
}

class Min10RemoveFromSpecListEvent extends Min10Event {}

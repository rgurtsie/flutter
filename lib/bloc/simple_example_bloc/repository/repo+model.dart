import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:http/http.dart';

class Model_first {
  final String name;
  final String email;
  final String body;

  Model_first({required this.name, required this.email, required this.body});

  factory Model_first.fromJson(Map<String, dynamic> json) {
    return Model_first(
        name: json['name'], email: json['email'], body: json['body']);
  }
}

class Repository_first {
  Uri api = Uri.parse('https://jsonplaceholder.typicode.com/comments');

  Future getData() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List request = jsonDecode(response.body);
      final list = request.map(((json) => Model_first.fromJson(json))).toList();
      return list;
    } else {
      return Exception(response.reasonPhrase);
    }
  }
}

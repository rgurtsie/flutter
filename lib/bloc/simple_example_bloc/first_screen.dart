import 'package:bc/bloc/simple_example_bloc/repository/repo+model.dart';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/first_bloc.dart';

class MyFirst extends StatelessWidget {
  const MyFirst({super.key});

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => Repository_first(),
      child: MaterialApp(home: FirstScreen()),
    );
  }
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          FirstBloc(RepositoryProvider.of<Repository_first>(context))
            ..add(FirstEvent()),
      child: Scaffold(
          body: BlocBuilder<FirstBloc, FirstState>(
              builder: (context, state) => getWidget(state))),
    );
  }

  Widget getWidget(state) {
    if (state is FirstloadingState) {
      return Center(child: CircularProgressIndicator());
    } else if (state is FirstloadedState) {
      final list = state.listData;
      return ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Card(
                child: Column(
              children: [Text(list[index].name),],
            ));
          });
    } else if (state is FirstErrorloadingState) {
      return Center(child: Text('You are wrong !'));
    }
    return Container();
  }
}

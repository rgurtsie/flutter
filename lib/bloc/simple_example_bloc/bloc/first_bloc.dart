
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../repository/repo+model.dart';

part 'first_event.dart';
part 'first_state.dart';

class FirstBloc extends Bloc<FirstEvent, FirstState> {
  Repository_first _repository_first;
  FirstBloc(this._repository_first) : super(FirstloadingState()) {
    on<FirstEvent>((event, emit) async {
      emit(FirstloadingState());

      try {
        final listData = await _repository_first.getData();
        emit(FirstloadedState(listData));
      } catch (error) {
        emit(FirstErrorloadingState());
      }

      // Stream<FirstState> mapEventToState(FirstEvent event, Emitter<FirstState emit>)async*{
      //   if(event is FirstloadingState){
      //     emit()
      //   }

      // }
    });
  }
}

part of 'first_bloc.dart';

abstract class FirstState extends Equatable {
  const FirstState();

  @override
  List<Object> get props => [];
}

class FirstloadingState extends FirstState {}

class FirstloadedState extends FirstState {
  final List<Model_first> listData;

  FirstloadedState(this.listData);
    @override
  List<Object> get props => [listData];
}

class FirstErrorloadingState extends FirstState {}

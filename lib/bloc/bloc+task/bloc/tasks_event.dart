part of 'tasks_bloc.dart';

abstract class TasksEvent extends Equatable {
  const TasksEvent();

  @override
  List<Object> get props => [];
}

class TasksAddEvent extends TasksEvent {
  final Model task;

 const TasksAddEvent(this.task);
  @override
  List<Object> get props => [task];
}

class TasksUpdateEvent extends TasksEvent {
  final Model task;

const  TasksUpdateEvent(this.task);

  @override
  List<Object> get props => [task];
}

class TasksDeletedEvent extends TasksEvent {
  final Model task;

const  TasksDeletedEvent(this.task);

  @override
  List<Object> get props => [task];
}



part of 'tasks_bloc.dart';

class TasksState extends Equatable {
  final List<Model> allTasks;

  const TasksState({this.allTasks = const <Model>[]});

  @override
  List<Object> get props => [allTasks];

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'allTasks': allTasks.map((x) => x.toMap()).toList(),
    };
  }

  factory TasksState.fromMap(Map<String, dynamic> map) {
    return TasksState(
      allTasks: List<Model>.from((map['allTasks'] as List<int>).map<Model>((x) => Model.fromMap(x as Map<String,dynamic>),),),
    );
  }

 
}

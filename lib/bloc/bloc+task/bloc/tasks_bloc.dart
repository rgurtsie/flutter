import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../model/model.dart';

part 'tasks_event.dart';
part 'tasks_state.dart';

class TasksBloc extends Bloc<TasksEvent, TasksState> {
  TasksBloc() : super(const TasksState()) {
    on<TasksAddEvent>(_onAddTasksEvent);
    on<TasksUpdateEvent>(_onUpdateTaskEvent);
    on<TasksDeletedEvent>(_onDeleteTasksEvent);
  }

  void _onAddTasksEvent(TasksAddEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    emit(TasksState(allTasks: (state.allTasks).toList()..add(event.task)));
  }

  void _onUpdateTaskEvent(TasksUpdateEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    final task = event.task;
    final int index = state.allTasks.indexOf(task);
    List<Model> allTasks = List.from(state.allTasks)..remove(task);
    task.isDone == false
        ? allTasks.insert(index, task.copyWith(isDone: true))
        : allTasks.insert(index, task.copyWith(isDone: false));

    emit(TasksState(allTasks: allTasks));
  }

  void _onDeleteTasksEvent(TasksDeletedEvent event, Emitter<TasksState> emit) {
    final state = this.state;
    final task = event.task;
    List<Model> allTasks = List.from(state.allTasks)..remove(task);

    emit(TasksState(allTasks: allTasks));
  }
}

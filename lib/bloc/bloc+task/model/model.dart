import 'package:equatable/equatable.dart';

class Model extends Equatable{
  final String title;
  bool? isDone;
  bool? isDeleted;

  Model({required this.title, this.isDeleted, this.isDone}) {
    isDeleted = isDeleted ?? false;
    isDone = isDone ?? false;
  }

  Model copyWith({
    String? title,
    bool? isDone,
    bool? isDeleted,
  }) {
    return Model(
      title: title ?? this.title,
      isDone: isDone ?? this.isDone,
      isDeleted: isDeleted ?? this.isDeleted,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'title': title,
      'isDone': isDone,
      'isDeleted': isDeleted,
    };
  }

  factory Model.fromMap(Map<String, dynamic> map) {
    return Model(
      title: map['title'] as String,
      isDone: map['isDone'] != null ? map['isDone'] as bool : null,
      isDeleted: map['isDeleted'] != null ? map['isDeleted'] as bool : null,
    );
  }
  
  @override

  List<Object?> get props => [isDeleted,isDone,title];
}

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/tasks_bloc.dart';
import 'model/model.dart';

class MyTasks extends StatelessWidget {
  const MyTasks({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TasksBloc(),
      child: MaterialApp(
        home: TaskScreen(),
      ),
    );
  }
}

class TaskScreen extends StatefulWidget {
  TaskScreen({super.key});

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  TextEditingController titleEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TasksBloc, TasksState>(
      builder: (context, state) {
        List<Model> listTasks = state.allTasks;
        return GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Scaffold(
            appBar: AppBar(),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Chip(
                    label: Text('Tasks: '),
                  ),
                ),
                TextField(
                  controller: titleEditingController,
                  decoration: InputDecoration(
                      label: Text('title'), border: OutlineInputBorder()),
                ),
                ElevatedButton(
                    onPressed: () {
                      var task = Model(title: titleEditingController.text);
                      if (titleEditingController.text.isNotEmpty) {
                        context.read<TasksBloc>().add(TasksAddEvent(task));

                        titleEditingController.clear();
                      } else {
                        return;
                      }
                    },
                    child: Text('add task')),
                Expanded(
                    child: ListView.builder(
                        itemCount: listTasks.length,
                        itemBuilder: (context, index) {
                          var task = listTasks[index];
                          return ListTile(
                            title: Text(task.title),
                            trailing: Checkbox(
                              onChanged: (value) {
                                context
                                    .read<TasksBloc>()
                                    .add(TasksUpdateEvent(task));
                              },
                              value: task.isDone,
                            ),
                            onLongPress: () => context
                                .read<TasksBloc>()
                                .add(TasksDeletedEvent(task)),
                          );
                        }))
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {},
              tooltip: 'Add Task',
              child: Icon(Icons.add),
            ),
          ),
        );
      },
    );
  }
}

import 'package:bc/bloc/tutorial_bloc/bloc/user_widget_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'bloc_1/counter_widget_for_bloc.dart';

class MyWidgetForBloc extends StatelessWidget {
  const MyWidgetForBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<CounterWidgetForBloc>(
              create: (context) => CounterWidgetForBloc()),
          BlocProvider<UserWidgetBloc>(create: (context) => UserWidgetBloc())
        ],
        child: MaterialApp(
          theme: ThemeData.dark(),
          home: WidgetForBloc(),
        ));
  }
}

class WidgetForBloc extends StatelessWidget {
  const WidgetForBloc({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 100.0),
            child: BlocBuilder<CounterWidgetForBloc, int>(
              builder: (context, state) {
                return Text(state.toString());
              },
            ),
          ),
          SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: () => context
                      .read<CounterWidgetForBloc>()
                      .add(CounterIncWidgetForEvent()),
                  child: Text('+')),
              ElevatedButton(
                  onPressed: () => context
                      .read<CounterWidgetForBloc>()
                      .add(CounterDecWidgetForEvent()),
                  child: Text('-')),
              ElevatedButton(
                  onPressed: () {
                    final state = context.read<CounterWidgetForBloc>().state;

                    context
                        .read<UserWidgetBloc>()
                        .add(UserGetWidgetEvent(state));
                  },
                  child: Text('person')),
              ElevatedButton(
                  onPressed: () {
                    final state = 0;
                    context
                        .read<UserWidgetBloc>()
                        .add(UserGetWidgetEvent(state));
                  },
                  child: Text('clean')),
            ],
          ),
          SizedBox(
            height: 50.0,
          ),
          BlocBuilder<UserWidgetBloc, UserWidgetState>(
            builder: (context, state) {
              return widget(state);
            },
          )
        ],
      ),
    );
  }

  Widget widget(state) {
    if (state is UserLoadingWidgetState) {
      return state.widget;
    } else if (state is UserLoadedWidgetState) {
      return Expanded(
          child: ListView.builder(
        itemCount: state.listData.length,
        itemBuilder: (context, index) {
          return Card(
            child: Text(state.listData[index].name),
          );
        },
      ));
    }
    return Container();
  }
}

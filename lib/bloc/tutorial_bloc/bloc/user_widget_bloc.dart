import 'package:bc/bloc/tutorial_bloc/model/model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'user_widget_event.dart';
part 'user_widget_state.dart';

class UserWidgetBloc extends Bloc<UserWidgetEvent, UserWidgetState> {
  UserWidgetBloc() : super(UserWidgetInitial()) {
    on<UserGetWidgetEvent>(_onUserGetWidgetEvent);
  }
  _onUserGetWidgetEvent(UserGetWidgetEvent event, Emitter emit) async {
    emit(UserLoadingWidgetState(Center(child: CircularProgressIndicator())));
    await Future.delayed(Duration(seconds: 1));
    print(event.count);
    final users = List.generate(
        event.count, (index) => Model_widgetforbloc('name ${index}'));
    print(users.length);
    emit(UserLoadedWidgetState(users));
  }
}

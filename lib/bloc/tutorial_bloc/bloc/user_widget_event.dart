part of 'user_widget_bloc.dart';

abstract class UserWidgetEvent extends Equatable {
  const UserWidgetEvent();

  @override
  List<Object> get props => [];
}

class UserGetWidgetEvent extends UserWidgetEvent {
  final int count;

  UserGetWidgetEvent(this.count);
}

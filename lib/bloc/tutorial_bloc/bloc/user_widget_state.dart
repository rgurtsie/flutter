part of 'user_widget_bloc.dart';

abstract class UserWidgetState extends Equatable {
  const UserWidgetState();

  @override
  List<Object> get props => [];
}

class UserWidgetInitial extends UserWidgetState {}

class UserLoadingWidgetState extends UserWidgetState {
  final Widget widget;

  UserLoadingWidgetState(this.widget);
}

class UserLoadedWidgetState extends UserWidgetState {
  final List<Model_widgetforbloc> listData;

  UserLoadedWidgetState(this.listData);
}

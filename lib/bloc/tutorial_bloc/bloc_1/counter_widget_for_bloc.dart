import 'package:bc/bloc/tutorial_bloc/model/model.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'counter_widget_for_event.dart';

class CounterWidgetForBloc extends Bloc<CounterWidgetForEvent, int> {
  CounterWidgetForBloc() : super(0) {
    on<CounterIncWidgetForEvent>(_onCounterIncWidgetForEvent);
    on<CounterDecWidgetForEvent>(__onCounterDecWidgetForEvent);
  }
  _onCounterIncWidgetForEvent(CounterIncWidgetForEvent event, Emitter emit) {
    emit(state + 1);
  }

  __onCounterDecWidgetForEvent(event, emit) {
    if (state <= 0) {
      return;
    } else {
      return emit(state - 1);
    }
  }
}

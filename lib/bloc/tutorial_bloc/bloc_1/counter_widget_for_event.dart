part of 'counter_widget_for_bloc.dart';

abstract class CounterWidgetForEvent extends Equatable {
  const CounterWidgetForEvent();

  @override
  List<Object> get props => [];
}

class CounterIncWidgetForEvent extends CounterWidgetForEvent{}
class CounterDecWidgetForEvent extends CounterWidgetForEvent{}

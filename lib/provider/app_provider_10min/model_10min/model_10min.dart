import 'dart:math';

import 'package:flutter/cupertino.dart';

final List<Model_10min> listData = List.generate(
    100,
    (index) => Model_10min(
        title: "Move ${index}",
        duration: "${Random().nextInt(100) + 60} minutes"));

class Model_10min {
  final String title;
  final String duration;

  Model_10min({required this.title, required this.duration});
}

class MovieProvider with ChangeNotifier {
  // полный список данных
  final List<Model_10min> _movies = listData;
  List<Model_10min> get movies => _movies;

//отвечает за сохранение и удаление данных
  final List<Model_10min> _myList = [];
  List<Model_10min> get myList => _myList;

//add special_List to Full_List
  void addToList(Model_10min movie) {
    _myList.add(movie);
    notifyListeners();
  }

// remove movie from special_List
  void removeFromList(Model_10min movie) {
    _myList.remove(movie);
    notifyListeners();
  }
  
}

import 'package:bc/provider/app_provider_10min/model_10min/model_10min.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import '2_app_widget.dart';

void main(List<String> args) {
  runApp(ChangeNotifierProvider<MovieProvider>(
    create: (context) => MovieProvider(),
    child: MyHomeApp(),
  ));
}

class MyHomeApp extends StatelessWidget {
  const MyHomeApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: HomeAppScreen(),
    );
  }
}

class HomeAppScreen extends StatefulWidget {
  const HomeAppScreen({super.key});

  @override
  State<HomeAppScreen> createState() => _HomeAppScreenState();
}

class _HomeAppScreenState extends State<HomeAppScreen> {
  @override
  Widget build(BuildContext context) {
    var movies = context.watch<MovieProvider>().movies;
    var special_movies = context.read<MovieProvider>().myList;
    return Scaffold(
      body: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(top: 50, bottom: 50),
            child: ElevatedButton(
                onPressed: () {
                  if (special_movies.length == 0) {
                    return;
                  } else {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Second_Widget()));
                  }
                },
                child: Text("Go to my list ${special_movies.length}")),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: movies.length,
                  itemBuilder: (context, index) {
                    final currentMovies = movies[index];
                    return Card(
                      elevation: 4,
                      child: ListTile(
                        title: Text(currentMovies.title),
                        subtitle: Text(currentMovies.duration),
                        trailing: IconButton(
                            onPressed: () {
                              if (!special_movies.contains(currentMovies)) {
                                context
                                    .read<MovieProvider>()
                                    .addToList(currentMovies);
                              } else {
                                context
                                    .read<MovieProvider>()
                                    .removeFromList(currentMovies);
                              }
                            },
                            icon: Icon(Icons.favorite,
                                color: special_movies.contains(currentMovies)
                                    ? Colors.red
                                    : Colors.white)),
                      ),
                    );
                  }))
        ],
      ),
    );
  }
}

import 'package:bc/provider/app_provider_10min/model_10min/model_10min.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

class Second_Widget extends StatefulWidget {
  const Second_Widget({super.key});

  @override
  State<Second_Widget> createState() => _Second_WidgetState();
}

class _Second_WidgetState extends State<Second_Widget> {
  @override
  Widget build(BuildContext context) {
    var special_movies = context.watch<MovieProvider>().myList;

    return Scaffold(
      body: ListView.builder(
        itemCount: special_movies.length,
        itemBuilder: (context, index) {
          var currnetMovie = special_movies[index];
          return Card(
            elevation: 4,
            child: ListTile(
              title: Text(currnetMovie.title),
              subtitle: Text(currnetMovie.duration),
              trailing: TextButton(
                onPressed: () {
                  if (special_movies.length == 1) {
                    context.read<MovieProvider>().removeFromList(currnetMovie);
                    Navigator.pop(context);
                  } else {
                    context.read<MovieProvider>().removeFromList(currnetMovie);
                  }
                },
                child: Text(
                  'remove',
                  style: TextStyle(color: Colors.red),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}

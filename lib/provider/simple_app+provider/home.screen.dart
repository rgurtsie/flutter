import 'package:bc/provider/simple_app+provider/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';

import 'info_screen.dart';

class Home_screen extends StatelessWidget {
  const Home_screen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Home screen'),
            SizedBox(height: 30.0),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                      builder: (context) => Info_screen()));
                },
                child: Text('Info screen')),
            SizedBox(
              height: 30,
            ),
            Text(
              context.watch<Temperature>().temperature.toString()
              ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton(
                onPressed: () {
                  context.read<Temperature>().plusTemperature();
                },
                child: Text("+"))
          ],
        ),
      ),
    );
  }
}

import 'package:bc/bloc/bloc+calculator/calcul.dart';
import 'package:bc/bloc/simple_example_bloc/first_screen.dart';
import 'package:bc/provider/simple_app+provider/home.screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  int a = 1;
  List<String> list = ['hello', 'hi'];

  runApp(MultiProvider(providers: [
    Provider(create: (context) => a),
    Provider(create: (context) => list),
    ChangeNotifierProvider(create: (context) => Temperature())
  ], child: const MyWidget()));
}

class Temperature extends ChangeNotifier {
  int temperature = 0;
  void plusTemperature() {
    temperature++;
    notifyListeners();
  }
}

class MyWidget extends StatelessWidget {
  const MyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyCalculator(),
    );
  }
}

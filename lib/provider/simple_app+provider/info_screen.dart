import 'package:bc/provider/simple_app+provider/settings_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:provider/provider.dart';


class Info_screen extends StatelessWidget {
  const Info_screen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Info screen'),
            Text(Provider.of<List<String>>(context)[1]),
            SizedBox(height: 30.0),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                      builder: (context) => Settings_screen()));
                },
                child: Text('Info screen'))
          ],
        ),
      ),
    );
  }
}



import 'package:bc/provider/simple_app+provider/home.screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Settings_screen extends StatelessWidget {
  const Settings_screen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Settings screen'+" "+ Provider.of<int>(context).toString()),
            SizedBox(height: 30.0),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute<void>(
                      builder: (context) => Home_screen()));
                },
                child: Text('Info screen'))
          ],
        ),
      ),
    );
  }
}
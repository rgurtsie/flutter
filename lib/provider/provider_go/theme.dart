import 'package:bc/provider/provider_go/style/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class MyTheme extends StatelessWidget {
  const MyTheme({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(theme: simpleTheme(), home: ThemeWidget());
  }
}

class ThemeWidget extends StatelessWidget {
  const ThemeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [
        SizedBox(
          height: 40.0,
        ),
        Text(
          'DATA',
          style: Theme.of(context).textTheme.headline6,
        ),
        Container(
          width: 100.0,
          child: Text(
              'I am come back! Actually, kmcksmdcksdmcpsmpmcpdmkmdmdkcmdkcmdkmcdkmckdmckdcmdkmcdkcmckdcmdmcdkcmd!',
              style: Theme.of(context).textTheme.bodyText1),
        )
      ],
    ));
  }
}

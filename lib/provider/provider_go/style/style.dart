import 'package:flutter/material.dart';

ThemeData simpleTheme() {
  return ThemeData(
    brightness: Brightness.dark,
    primaryColor: kPrimaryColor,
    textTheme: TextTheme(
        headline6: TextStyle(fontSize: 30, color: kRedColor),
        bodyText1: TextStyle(fontSize: 20, color: kPrimaryColor)),
  );
}

Color kPrimaryColor = Colors.purple;
Color kRedColor = Colors.red;

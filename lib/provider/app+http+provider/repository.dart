import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';

class Http_repository {
  Uri api = Uri.parse("https://jsonplaceholder.typicode.com/posts");
  Future getData() async {
    Response response = await get(api);
    if (response.statusCode == 200) {
      final List request = jsonDecode(response.body);
      return request.map(((e) => HttpModel.fromJson(e))).toList();
    } else {
      return Exception(response.reasonPhrase);
    }
  }

  Future deletedData({required int data}) async {
    Uri apiDelete =
        Uri.parse("https://jsonplaceholder.typicode.com/posts/${data}");
    Response response = await delete(apiDelete);
    if (response.statusCode == 200) {
      // ScaffoldMessenger.of(context).showSnackBar(
      //     SnackBar(content: Text("${data} user was deleted")));
      print('Good !');

    } else {
      return Exception(response.reasonPhrase);
    }
  }

  Future postData() async {
    Uri apiPost = Uri.parse("https://jsonplaceholder.typicode.com/posts");
    Response response = await post(apiPost);
    if (response.statusCode == 200) {
      
      print('Good Post!');
    } else {
      return Exception(response.reasonPhrase);
    }
  }
}

class HttpModel {
  final int userId;
  final String title;

  HttpModel({required this.userId, required this.title});

  factory HttpModel.fromJson(Map<String, dynamic> json) {
    return HttpModel(userId: json['userId'], title: json['title']);
  }
}

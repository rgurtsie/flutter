import 'package:bc/provider/app+http+provider/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/http_p_bloc.dart';

class MyHttpWidget extends StatelessWidget {
  const MyHttpWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => HttpPBloc(Http_repository()))
      ],
      child: const HttpWidget(),
    ));
  }
}

class HttpWidget extends StatelessWidget {
  const HttpWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HttpPBloc, HttpPState>(
      builder: (context, state) {
        return Scaffold(
          body: Container(
            margin: EdgeInsets.only(top: 30),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: 30,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                          onPressed: () => context.read<HttpPBloc>()
                            ..add(HttpPLoadedEvent()),
                          child: Text('loaded')),
                      ElevatedButton(
                          onPressed: () =>
                              context.read<HttpPBloc>()..add(HttpPostEvent()),
                          child: Text('post'))
                    ],
                  ),
                ),
                widget(state, context)
              ],
            ),
          ),
        );
      },
    );
  }

  Widget widget(state, context) {
    if (state is HttpPLoadedState) {
      return Expanded(
          child: ListView.builder(
              itemCount: state.listData.length,
              itemBuilder: (context, index) => Card(
                    elevation: 4,
                    child: Column(
                      children: [
                        Text(state.listData[index].title),
                        TextButton(
                          onPressed: () => context.read<HttpPBloc>()
                            ..add(HttpPDeletedEvent(index)),
                          child: Text('delete'),
                        ),
                      ],
                    ),
                  )));
    } else if (state is HttpPDeletedState) {
      return state.widget;
    } else if (state is HttpPostState) {
      return Center(child: Text(state.text));
    }
    return Text('');
  }
}

import 'package:bc/provider/app+http+provider/repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'http_p_event.dart';
part 'http_p_state.dart';

class HttpPBloc extends Bloc<HttpPEvent, HttpPState> {
  final Http_repository _http_repository;
  HttpPBloc(this._http_repository) : super(HttpPInitialState()) {
    on<HttpPLoadedEvent>(_onHttpLoadedState);
    on<HttpPDeletedEvent>(_onHttpPDeletedEvent);
    on<HttpPostEvent>(_onHttpPostEvent);
  }

  _onHttpLoadedState(HttpPLoadedEvent event, Emitter emit) async {
    emit(HttpPInitialState());
    try {
      final data = await _http_repository.getData();
      emit(HttpPLoadedState(data));
    } catch (error) {
      emit(HttpErrorLoadedState('You are wrong !'));
    }
  }

  _onHttpPDeletedEvent(HttpPDeletedEvent event, Emitter emit) async {
    try {
      final deleted = await _http_repository.deletedData(data: event.number);
      emit(HttpPDeletedState(Text("${event.number} was deleted")));
    } catch (error) {
      emit(HttpErrorLoadedState('You are wrong !'));
    }
  }

  _onHttpPostEvent(HttpPostEvent event, Emitter emit) async {
    final postData = await _http_repository.postData();
    emit(HttpPostState('Richard, you can do anything !'));
  }
}

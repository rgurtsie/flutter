// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'http_p_bloc.dart';

abstract class HttpPState extends Equatable {
  const HttpPState();

  @override
  List<Object> get props => [];
}

class HttpPInitialState extends HttpPState {}

class HttpPostState extends HttpPState {
  final String text;

  HttpPostState(this.text);
}

class HttpPLoadedState extends HttpPState {
  final List<HttpModel> listData;
  HttpPLoadedState(
    this.listData,
  );
}

class HttpPDeletedState extends HttpPState {
  final Widget widget;

  HttpPDeletedState(this.widget);
}

class HttpErrorLoadedState extends HttpPInitialState {
  final String error;

  HttpErrorLoadedState(this.error);
}

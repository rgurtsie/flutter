part of 'http_p_bloc.dart';

abstract class HttpPEvent extends Equatable {
  const HttpPEvent();

  @override
  List<Object> get props => [];
}

class HttpPostEvent extends HttpPEvent {}

class HttpPLoadedEvent extends HttpPEvent {}

class HttpPDeletedEvent extends HttpPEvent {
  final int number;

  HttpPDeletedEvent(this.number);
}
